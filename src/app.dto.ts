import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  Min,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class FilesDto {
  @IsArray()
  numbers: number[];

  @IsNumber()
  @IsOptional()
  numbersSum?: number;

  @IsNumber()
  @IsOptional()
  minNumber?: number;

  @IsNumber()
  @IsOptional()
  maxNumber?: number;
}

export class FileDto {
  @IsNumber()
  number: number;
}

export class FilesSaveCommand {
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    description: 'Number of files to create',
  })
  @Min(1)
  numberOfFiles: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    description: 'Maximum value of numbers that will be created in the files',
  })
  @Min(1)
  maxNumber: number;
}
