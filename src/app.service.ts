import { Injectable, NotFoundException } from '@nestjs/common';
import { FilesSaveCommand, FilesDto, FileDto } from './app.dto';
import {
  fileExists,
  readFile,
  readFilesFromDir,
  writeToFile,
} from './common/common.fileService';

@Injectable()
export class AppService {
  async getAllNumbers(): Promise<FilesDto> {
    const numbers = [];

    const filesNames = await readFilesFromDir();
    for (const fileName of filesNames) {
      const number = await readFile(fileName);
      numbers.push(number);
    }

    const fileDto = new FilesDto();
    fileDto.numbers = numbers;

    if (numbers.length) {
      fileDto.numbersSum = numbers.reduce((accumulator, value) => {
        return accumulator + value;
      }, 0);
      fileDto.maxNumber = Math.max(...numbers);
      fileDto.minNumber = Math.min(...numbers);
    }

    return fileDto;
  }

  async getFileNumber(fileName: string): Promise<FileDto> {
    if (!fileExists(fileName)) {
      throw new NotFoundException(`File with name: ${fileName} doesn't exists`);
    }
    const number = await readFile(fileName);

    return { number };
  }

  async createFilesWithNumbers(
    filesSaveCommand: FilesSaveCommand,
  ): Promise<void> {
    for (let i = 1; i <= filesSaveCommand.numberOfFiles; i++) {
      const randNumber = Math.floor(Math.random() * filesSaveCommand.maxNumber);
      writeToFile(i, randNumber);
    }
  }
}
