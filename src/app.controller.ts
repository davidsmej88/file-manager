import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
} from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiExtraModels,
  ApiBadRequestResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
} from '@nestjs/swagger';
import { ApiOkOneResponse } from './common/common.decorators';
import {
  NoContentResponse,
  OkManyResponse,
  OkOneResponse,
} from './common/common.apiResponse';
import { FilesSaveCommand, FilesDto, FileDto } from './app.dto';
import { AppService } from './app.service';

@Controller('files')
@ApiTags('Files')
@ApiExtraModels(
  OkOneResponse,
  FilesDto,
  OkManyResponse,
  FilesSaveCommand,
  FileDto,
)
@ApiBearerAuth()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @ApiOkOneResponse(FilesDto)
  async getAllNumbers(): Promise<OkOneResponse<FilesDto>> {
    const response = new OkOneResponse<FilesDto>();
    response.data = await this.appService.getAllNumbers();

    return response;
  }

  @Get(':fileName')
  @ApiNotFoundResponse({ description: "File doesn't exists" })
  @ApiOkOneResponse(FileDto)
  async getFileNumber(
    @Param('fileName') fileName: string,
  ): Promise<OkOneResponse<FileDto>> {
    const response = new OkOneResponse<FileDto>();
    response.data = await this.appService.getFileNumber(fileName);

    return response;
  }

  @Post()
  @ApiBadRequestResponse({ description: 'Incorrect parameters.' })
  @ApiNoContentResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  async createFilesWithNumbers(
    @Body() filesSaveCommand: FilesSaveCommand,
  ): Promise<NoContentResponse> {
    await this.appService.createFilesWithNumbers(filesSaveCommand);

    return new NoContentResponse();
  }
}
