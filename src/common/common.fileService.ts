import * as fs from 'fs';
import { FILE_DIR_PATH } from '../../config/config';

export const writeToFile = (fileName: number, randNumber: number) => {
  try {
    fs.writeFileSync(
      `${FILE_DIR_PATH}/${fileName}`,
      JSON.stringify(randNumber),
    );
  } catch (e) {
    throw new Error(`During creating files something get wrong ${e}`);
  }
};

export const readFile = async (fileName: string): Promise<number> => {
  try {
    const number = await fs.promises.readFile(
      `${FILE_DIR_PATH}/${fileName}`,
      'utf8',
    );

    return +number;
  } catch (e) {
    throw new Error(`During reading file something get wrong ${e}`);
  }
};

export const readFilesFromDir = async (): Promise<string[]> => {
  try {
    return await fs.promises.readdir(FILE_DIR_PATH);
  } catch (e) {
    throw new Error(`During reading files something get wrong ${e}`);
  }
};

export const fileExists = (fileName: string): boolean => {
  return fs.existsSync(`${FILE_DIR_PATH}/${fileName}`);
};
