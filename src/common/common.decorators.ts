/***************************************************************************
 * Method decorators
 **************************************************************************/
import { applyDecorators, Type } from '@nestjs/common';
import { ApiOkResponse, getSchemaPath } from '@nestjs/swagger';
import { OkOneResponse } from './common.apiResponse';

export const ApiOkOneResponse = <TModel extends Type<any>>(model: TModel) => {
  return applyDecorators(
    ApiOkResponse({
      schema: {
        allOf: [
          {
            type: 'object',
            required: ['data'],
            properties: {
              data: {
                $ref: getSchemaPath(model),
              },
            },
          },
          { $ref: getSchemaPath(OkOneResponse) },
        ],
      },
    }),
  );
};
