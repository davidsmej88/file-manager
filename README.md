# File Manager App
Small project to manage files in Nest.js framework. After running project, API doc will be available [here](http://localhost:3000/api)

## Get started
1. Copy `.env.example` to `.env` in api folder:
    * FILE_DIR_PATH - if you let it empty, default value './' will be set
2. Run `npm install` in api folder and client folder.
3. Run `npm run start` for api